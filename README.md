# TODO-s
  * HIGH_PRIORITY
  * NORMAL_PRIORITY
  * LOW_PRIORITY
    - extern helyett mást  
    - LOD for better preformance  
    - GNU chess  
    - chess.c szétszervez  
    - logger  https://logging.apache.org/log4cxx/latest_stable/index.html  


# TODO-s FROM PILLER 
  * HIGH_PRIORITY
  * NORMAL_PRIORITY
  * LOW_PRIORITY
    - Rengeteg a komment és a formázási hiba.   
    - A repo és úgy általában a program mérete elég nagy. Nem lenne baj, ha 50 MB felé nem nagyon menne az egész.  
    - A forráskódban az elnevezéseknek angolul kellene szerepelni.  
    - A kódban a repeptitív, adattárolásra emlékeztető kódrészeket külön adatszerkezetbe, adatfájlba kell kiszervezni.  
    - A bool típus kezeléséhez az stdbool-os include-ok teljesen rendben vannak, viszont a TRUE és FALSE értékek definícióját valamiért nem találta a program. (Egyelőre megadtam több helyen is a definíciót hozzá, de lehet a kisbetűs, C++ szerű változat egyszerűbb lesz így.)  
    - A modellek betöltésénél kis/nagybetű probléma volt. A textúrák betöltésénél szintén.  
    - Érdemes minden olyan helyre const-ot írni a paraméterezésnél, ahol az adott elemen nem kell változtatni (és nem számérték).  
    - A tábla inicializálása futás szempontjából így biztosan hatékonyabb, viszont a kód elegánsabb lehet valamilyen ciklusos megoldással.  
    - A chess.c-re egy kis refaktorálás ráfér majd. Komoly számítások teljesen jól implementálásra kerültek, viszont pár helyen majd lehet még rajta egyszerűsíteni.  

# FUTURE POSSIBILITES 
  * Sáncolás    
  * Bábu becserélés    
  * Eredményjelző (leütött bábuk)    
  * Lépés visszavonása    
  * GNU CHESS (AI)

# GIT LINK
	https://gitlab.com/szacskesz/BP4_Grafika_Bead  

# LICENCE
| Description            | Link                                                                                      |
| ------                 | ------                                                                                    |
| Board texture:         | https://www.chessusa.com/mm5/graphics/00000001/30-325/30-325_4_large.jpg                  |
| Skyshphere texture:    | http://hdrmaps.com/freebies/hdri-sky-1553                                                 |
| Keyboard image:        | https://commons.wikimedia.org/wiki/File:ANSI_Keyboard_Layout_Diagram_with_Form_Factor.svg |
| Mouse image:           | https://pixabay.com/hu/eg%C3%A9r-perif%C3%A9ri%C3%A1s-eszk%C3%B6z-r%C3%A1mutatva-34458/   |
| Figure models:         | https://www.turbosquid.com/3d-models/free-chessset-blender-cycles-3d-model/973464         |
| Texture.c, Texture.h   | http://www.uni-miskolc.hu/~matip/grafika/                                                 |
| Model.c, Model.h       | http://www.uni-miskolc.hu/~matip/grafika/                                                 |
| Draw.c, Draw.h         | http://www.uni-miskolc.hu/~matip/grafika/                                                 |

# DONE TODO-s
  - textures on cubes  
  - camera handling to camera.cpp  
  - load chess figures  
  - light effects  
  - light intensity +/-  
  - zooom saját func  
  - set models for color 3d position 3d size etc.  
  - background  
  - move objects    
  - click on obejcts    
  - merge light 0 and 1 to light0    
  - load chess figures    
  - textures load to external file    
  - draw. c h    
  - textures on cubes    
  - light should not rotate with the camera    
  - light effects    
  - light intensity +/-    
  - camera handling to camera.c    
  - zooom saját func    
  - constructor for light    
  - set models/structs for color, 3dposition, 3dsize etc.    
  - create headers     
  - háttér    
  - egy fény + minden komponenst lehessen állítani    
  - texture no need for set filename, load it directly    
  - minden bábura kiszámolni hova léphet    
  - lépés után ezt frissíteni ^      
  - lépést ne engedje ha sakkba kerülne miatta    
  - sakk: végig megyünk minden ellenséges bábun és megnézzük hogy tud-e lépni a király helyére    
  - sakk-matt: ha sakk majd végig megyünk az összes szövetséges bábun és azok lépésein ha egyik sem eredményez nem sakk   -   - állapotot akkor matt    
  - király királyhoz nem léphet  
  - modell betöltés modellenként csak egyszer kellene és nem figuránként x szer   
  - chess.h doc-s befejez  
  - animációk mozgáshoz    
  - animáció befejezése után legyen a moves calculation  
  - animáció alatt nem lehet kattintani  
  - animáció emelkedjen is  
  - nimáció legyen görbén  
  - F1 help menu elkészít    
  - felh felület sakk állapotokhoz sakk, sakkmatt, döntetlen    
  - configuráció debug és fps kiírásához

# DONE TODO-s FROM PILLER
  - A jegyzékek nevét jobb lenne egységesen megoldani. Általában a végig kisbetűset preferálom, de ha nagybetűvel kezded az sem gond, csak egységes legyen.  
  - A textúrák között ahogy látom a modellek is benne vannak.  
  - A dll nem kell a repo-ba.  
  - A shell.bat sem kell bele.  
  - A c és h fájlokat jobb lenne különszedni.  
  - A modellek betöltéséhez érdemes lenne valamilyen adatszerkezetet használni, hogy ne a kódba legyen soronként megadva, hogy mit és honnan töltsön be.  
  - A main.c az így túl hosszú. Ki kellene szervezni az inicializálást, a callback-eket.  
  - Az includes.h-t nem tartom indokoltnak.  
  - A fejlécekbe jó lenne valamilyen dokumentációs komment féle.  
  - A program forráskódját make paranccsal lefordítható formában, illetve a futtatáshoz szükséges fájlokat (például: képek, modellfájlok) kell átküldeni tömörített (zip, tar.gz) fájlként.  
  - A fájlt valahová fel kellene tenni (például: saját weboldal, DropBox, OneDrive), nem pedig csatolmányként küldeni. Ha valaki GitHub-ra, GitLab-ra vagy BitBucket-ra rakja fel a kódját, az csak pozitívum. (Abban az esetben elég a repo linkje.)    - A forrásfájlok kiterjesztése C nyelv használata esetén c legyen, C++ nyelv használata esetén pedig cpp.  
  - A textúrákhoz PNG vagy JPEG kiterjesztésű fájlokat érdemes használni.  
  - A main függvényt tartalmazó fájl neve main.c legyen (C++ esetén main.cpp).  
  - A help menü is kifejezetten tetszetős/elegáns lett. :)  
  - A chess.c drawCuboid függvényét valahogy érdemes lehet egyszerűsíteni, kivenni belőle az ismétlődő részeket.  
  - A camera.c-ben a M_PI-s vizsgálat úgy jó, de lehet elég hagyatkozni arra is, hogy a math.h-ban benne van. (Gyanítom, hogy nem véletlenül került így bele, tehát lehet, hogy a Windows-os változatban nem mindig van meg.)  - nincs benne mindig :D
  - Törekedni kell arra, hogy a program warning nélkül forduljon.  
  - A callback-es gombok esetében valószínűleg van konstans definiálva a button változó lehetséges értékeire is. (Meg kell néznem, de tipikusan szokott lenni.)  
  - Minimális számú globális változót kellene használni.  
  - Az ablak átméretezéséhez érdemes lehet valamilyen rögzített képarányt megadni. (Minimális változtatás, készen meg van hozzá a számítás például a cube-os mintában.)
  - Ha a forráskódhoz tartozik valamilyen külső forrásból származó kódrész, akkor azt a megfelelő licensz mellett fel kell tüntetni!  
  - A fejléc állományokba dokumentált formában kerüljön az interfészek leírása. A forrásállományba ideális esetben nem szükségesek megjegyzések.  
  - A callbacks.c -ben lévő extern-eket érdemes lenne kiváltani egy, például Game és/vagy Scene struktúrával. Logikailag is jobb, ha össze vannak fogva, és akkor úgy elég csak, mint mezőkre hivatkozni a globális struktúra változón keresztül.  
  - A header-ökben a paraméterezés nélkül kissé hosszúra sikerült. Olyankor célszerűbb több sorba törni, vagy megnézni, hogy melyek azok a paraméterek, amelyek kiemelhetők egy struktúrába.  
  - A hosszú paraméterezéseket több sorba célszerű törni. 