#include <GL/glut.h>
#include <stdio.h>

#include "lightning.h"
#include "utils.h"


void initLight(
	Light* light,
	double intensity,
	ColorRGB color,
	Position3D position,
	GLenum id
) {
	light->intensity = intensity;
	light->color = color;
	light->position = position;
	light->id = id;
	
	glEnable(GL_LIGHTING);
    glEnable(light->id);	
	GLfloat glob_ambient[] = {0.0, 0.0, 0.0, 1.0};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, glob_ambient);
}
 
void changeLightsIntensity(unsigned char key, Light lights[], int size) {
	for(int i = 0; i < size; i++)
	{
		changeLightIntensity(key, &lights[i]);
	}
	
}
 
void changeLightIntensity(unsigned char key, Light* light) {
	switch (key) {
    case '+':
        light->intensity += 0.05;
        break;
    case '-':
        light->intensity -= 0.05;
        break;
    }

	if (light->intensity > 1.5)
	{
		light->intensity = 1.5;
	}
	if (light->intensity < 0.0)
	{
		light->intensity = 0.0;
	}
}

void renderLights(Light lights[], int size) {
	for(int i = 0; i < size; i++)
	{
		renderLight(&lights[i]);
	}
	
}

void renderLight(Light* light) {
	GLfloat position[] = 
	{
		light->position.x,
		light->position.y,
		light->position.z, 
		0.0
	};
    GLfloat color[] = 
	{
		light->color.R * light->intensity,
		light->color.G * light->intensity,
		light->color.B * light->intensity,
		1.0
	}; 
	
	glLightfv(light->id, GL_POSITION, position);
	glLightfv(light->id, GL_AMBIENT, color);
	glLightfv(light->id, GL_DIFFUSE, color);
	glLightfv(light->id, GL_SPECULAR, color);
}

void printLightsDetail(Light lights[], int size) {
	for(int i = 0; i < size; i++)
	{
		printLightDetail(&lights[i]);
	}
	
}

void printLightDetail(Light* light) {
	printf("\n");
	printf("intensity: %f4\n", light->intensity);
	printf("color:R: %f4\n", light->color.R);
	printf("color:G: %f4\n", light->color.G);
	printf("color:B: %f4\n", light->color.B);
	printf("position:x: %f4\n", light->position.x);
	printf("position:y: %f4\n", light->position.y);
	printf("position:z: %f4\n", light->position.z);
	printf("id: %d\n", light->id);
}
