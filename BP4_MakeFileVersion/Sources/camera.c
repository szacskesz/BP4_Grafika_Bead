#include <GL/glut.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdbool.h>

#include "camera.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif


void init_camera( 
	Camera* camera,
	double zoomFactor,
	double horizontalAngleRadian,
	double verticalAngleRadian,
	bool startCapturingMouseMovement,
	double mouseOriginalPositionX,
	double mouseOriginalPositionY
)  { 
	camera->zoomFactor = zoomFactor;
	camera->horizontalAngleRadian = horizontalAngleRadian;
	camera->verticalAngleRadian = verticalAngleRadian;
	camera->startCapturingMouseMovement = startCapturingMouseMovement;
	camera->mouseOriginalPositionX = mouseOriginalPositionX;
	camera->mouseOriginalPositionY = mouseOriginalPositionY;
}

void renderCamera(Camera* camera) {
	gluLookAt(
		cos(camera->horizontalAngleRadian) * cos(camera->verticalAngleRadian) * camera->zoomFactor,
		sin(camera->verticalAngleRadian)* camera->zoomFactor,
		sin(camera->horizontalAngleRadian) * cos(camera->verticalAngleRadian) * camera->zoomFactor,
		0.0, 0.0, 0.0, 	// looking point
		cos(camera->horizontalAngleRadian) * cos(camera->verticalAngleRadian + (M_PI / 2)),
		sin(camera->verticalAngleRadian + (M_PI / 2)),
		sin(camera->horizontalAngleRadian) * cos(camera->verticalAngleRadian + (M_PI / 2))
	);
}

void processCameraRotationEvent(int x, int y, Camera* camera) {
	if (camera->startCapturingMouseMovement) {

		// update angles
		camera->horizontalAngleRadian += (x - camera->mouseOriginalPositionX) * 0.01;
		camera->verticalAngleRadian += (y - camera->mouseOriginalPositionY) * 0.01;
	
		// check limits 
		if (camera->verticalAngleRadian < 0)
		{
			camera->verticalAngleRadian = 0;
		}
		if (camera->verticalAngleRadian > (M_PI / 2) )
		{
			camera->verticalAngleRadian = (M_PI / 2);
		}
		
		// refresh mouse position
		camera->mouseOriginalPositionY = y;
		camera->mouseOriginalPositionX = x;
	}
}

void checkCameraRotationEvent(int button, int state, int x, int y, Camera* camera) {
	// only	start motion if the left button is pressed
	if (button == GLUT_MIDDLE_BUTTON) {

		// when the button is released
		if (state == GLUT_UP) {
			camera->startCapturingMouseMovement = FALSE;
		}
		// when the button is pressed
		if (state == GLUT_DOWN) {
			camera->startCapturingMouseMovement = TRUE;
			camera->mouseOriginalPositionX = x;
			camera->mouseOriginalPositionY = y;
		}
	}
}

void processCameraZoomEvent(int direction, Camera* camera) {
	camera->zoomFactor += 0.1 * direction;
	
	if(camera->zoomFactor  > 17.0) {
		camera->zoomFactor = 17.0;
	}
	if(camera->zoomFactor  < 7.0) {
		camera->zoomFactor = 7.0;
	}
}

void printCameraDetail(Camera* camera) {
	printf("\n");
	printf("zoomFactor: %f4\n", camera->zoomFactor);
	printf("horizontalAngleRadian: %f4\n", camera->horizontalAngleRadian);
	printf("verticalAngleRadian: %f4\n", camera->verticalAngleRadian);
	printf("startCapturingMouseMovement: %d\n", camera->startCapturingMouseMovement);
	printf("mouseOriginalPositionX: %f4\n", camera->mouseOriginalPositionX);
	printf("mouseOriginalPositionY: %f4\n", camera->mouseOriginalPositionY);
}
