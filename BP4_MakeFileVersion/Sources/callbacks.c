#include <GL/glut.h>
#include <stdbool.h>
#include <math.h>

#include "callbacks.h"
#include "lightning.h"
#include "camera.h"
#include "skysphere.h"
#include "chess.h"
#include "fps.h"
#include "screen_message.h"
#include "gamestate.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

extern GameState gameState;


void setCallbacks() {
	glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouseHandler);
    glutMotionFunc(mouseMotion);
	glutKeyboardFunc(keyboardNormalButtonHandler);
	glutSpecialFunc(keyboardSpecialButtonHandler);
	glutMouseWheelFunc(mouseScrollHandler); 	
    glutIdleFunc(idle);
}

void display() {
	glClearStencil(0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	
	renderCamera(&gameState.camera);
	renderLight(&gameState.light);
	renderSkysphere(&gameState.skysphere);
	renderBoard(gameState.chessBoardCells, gameState.selectedRowIndex, gameState.selectedColIndex, gameState.figures);
	if(gameState.existRunningAnimation == TRUE) {
		renderAnimatedMove(gameState.figures, gameState.animatedFigures);
	}

	if(gameState.shownMessage != NONE) {
		renderScreenMessage(gameState.textures[gameState.shownMessage]);
	}
		
	glPopMatrix();
	glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height) {
	int x, y, w, h;
    double ratio = (double)width / height;
	double viewportRatio =  gameState.viewportRatioW /  gameState.viewportRatioH;
	
    if (ratio > viewportRatio) {
        w = (int)((double)height * viewportRatio);
        h = height;
        x = (width - w) / 2;
        y = 0;
    }
    else {
        w = width;
        h = (int)((double)width / viewportRatio);
        x = 0;
        y = (height - h) / 2;
    }

    glViewport(x, y, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(gameState.viewportAspect, viewportRatio, 0.01, 10000.0);
}

void mouseHandler(int button, int state, int x, int y) {
	checkCameraRotationEvent(button, state, x, y, &gameState.camera);
	
	if(state == GLUT_DOWN &&  button == GLUT_LEFT_BUTTON )
	{
		double window_width = glutGet(GLUT_WINDOW_WIDTH);
		double window_height = glutGet(GLUT_WINDOW_HEIGHT);
	
		GLuint id; 
		glReadPixels(x, window_height - y - 1, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_INT, &id); 
			
		if(id>=1 && id <=64){
			int clickedRowIndex = (id-1) / 8;
			int clickedColIndex = (id-1) % 8;
			updateClickeableCells(
				gameState.chessBoardCells,
				gameState.figures, 
				&gameState.selectedRowIndex, 
				&gameState.selectedColIndex, 
				clickedRowIndex, 
				clickedColIndex, 
				&gameState.whosTurn, 
				&gameState.whiteCheck, 
				&gameState.blackCheck, 
				&gameState.existRunningAnimation, 
				gameState.animatedFigures
			);
		}
		
	}

	glutPostRedisplay();
}

void mouseScrollHandler(int wheel, int direction, int x, int y) {
	direction = direction * (-1);
	processCameraZoomEvent(direction, &gameState.camera);
	
	glutPostRedisplay();
}

void mouseMotion(int x, int y) {
	processCameraRotationEvent(x, y, &gameState.camera);
	
	glutPostRedisplay();
}

void idle() {
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	static int lastFrameTime = 0;
	
	if(gameState.showFps == TRUE) {
		printFPS(currentTime);
	}		
	int elapsedTimeInSec = (double)(currentTime - lastFrameTime) / 1000;
    lastFrameTime = currentTime;
	
	if(gameState.existRunningAnimation == TRUE) {
		updateAnimatedFiguresPosition(
			gameState.chessBoardCells, 
			gameState.figures, 
			&gameState.whosTurn, 
			gameState.animatedFigures, 
			currentTime, 
			&gameState.whiteCheck, 
			&gameState.blackCheck, 
			&gameState.existRunningAnimation, 
			&gameState.shownMessage, 
			&gameState.screenMessageStartTime
		);
		glutPostRedisplay();
	}
	if(gameState.shownMessage != NONE && gameState.shownMessage != HELP) {
		updateScreenMessageVisiblity(currentTime, &gameState.shownMessage, &gameState.screenMessageStartTime, gameState.screenMessageDuration);
		glutPostRedisplay();
	}
}

void keyboardNormalButtonHandler(unsigned char key, int x, int y) {
	changeLightIntensity(key, &gameState.light);

    glutPostRedisplay();
}

void keyboardSpecialButtonHandler (int key, int x, int y) {
	toggleHelpMenu(key, &gameState.shownMessage);
	
	glutPostRedisplay();
}
