#include <GL/glut.h>
#include <math.h>

#include "init.h"
#include "callbacks.h"
#include "gamestate.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif


void loadDefaultConfig(Config* config) {
	config->screenMessageDuration = 1500;
	config->dieAnimationSpeed  = 1.0;
	config->moveAnimationSpeed = 5.0;
	config->showFps = FALSE;
	config->lightPosX = 100.0;
	config->lightPosY = 100.0;
	config->lightPosZ = 100.0;
	config->lightColorRed = 1.0;
	config->lightColorGreen = 1.0;
	config->lightColorBlue = 1.0;
	config->screenWidth = 640;
	config->screenHeight = 480;
	config->viewportRatioW = 4.0;
	config->viewportRatioH = 3.0;
	config->viewportAspect = 50.0;
}

void loadConfigFromFile(Config* config, const char filename[100]) {
	char key[255];
	char value[255];
	char row[255];
    FILE *fptr;

    if ((fptr = fopen(filename, "r")) == NULL)
    {
		printf("-----------------------------------\n");
        printf("Warning!\n");    
		printf("Could not open configuration file: %s\n", filename);
		printf("\n");
		printf("Using default configuration instead\n");
		printf("-----------------------------------\n");
    } else {
		while( fgets( row, 1000, fptr ) != NULL) {
			if(row[strlen(row) - 1] == '\n') {
				row[strlen(row) - 1] = '\0';
			}
			sscanf(row, "%s = %s", key, value);

			if (strcmp(key,"ScreenMessageDuration") == 0) { 
				config->screenMessageDuration = atoi(value);
			}
			if(strcmp(key,"DieAnimationSpeed") == 0) {
				config->dieAnimationSpeed = atof(value);
			} 
			if(strcmp(key,"MoveAnimationSpeed") == 0) {
				config->moveAnimationSpeed = atof(value);
			} 
			if(strcmp(key,"ShowFps") == 0) {
				config->showFps = atoi(value);
			}
			if(strcmp(key,"LightPosX") == 0) {
				config->lightPosX = atof(value);
			}
			if(strcmp(key,"LightPosY") == 0) {
				config->lightPosY = atof(value);
			}
			if(strcmp(key,"LightPosZ") == 0) {
				config->lightPosZ = atof(value);
			}
			if(strcmp(key,"LightColorRed") == 0) {
				config->lightColorRed = atof(value);
			}
			if(strcmp(key,"LightColorGreen") == 0) {
				config->lightColorGreen = atof(value);
			}
			if(strcmp(key,"LightColorBlue") == 0) {
				config->lightColorBlue = atof(value);
			}
			if(strcmp(key,"ScreenWidth") == 0) {
				config->screenWidth = atoi(value);
			} 
			if(strcmp(key,"ScreenHeight") == 0) {
				config->screenHeight = atoi(value);
			} 
			if(strcmp(key,"ViewportRatioWidth") == 0) {
				config->viewportRatioW = atof(value);
			} 
			if(strcmp(key,"ViewportRatioHeight") == 0) {
				config->viewportRatioH = atof(value);
			} 
			if(strcmp(key,"ViewportAspectRatio") == 0) {
				config->viewportAspect = atof(value);
			} 
		}
		fclose(fptr);
	}
}

void initialize(GameState* gameState, Config config) {
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glEnable(GL_AUTO_NORMAL);
    glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0);
	
	initGameSate(gameState, config);
	
	display();
}

void initGameSate(GameState* gameState, Config config) {
	gameState->whosTurn = TEAM_WHITE;
	gameState->whiteCheck = FALSE;
	gameState->blackCheck = FALSE;
	gameState->existRunningAnimation = FALSE;
	gameState->selectedRowIndex = -1; //-1 means none
	gameState->selectedColIndex = -1; //-1 means none
	gameState->shownMessage = NONE;
	gameState->screenMessageDuration = config.screenMessageDuration; //in milisec
	gameState->animatedFigures[1].animationSpeed = config.moveAnimationSpeed;
	gameState->animatedFigures[0].animationSpeed = config.dieAnimationSpeed;
	gameState->showFps = config.showFps;
	gameState->viewportRatioW = config.viewportRatioW;
	gameState->viewportRatioH = config.viewportRatioH;
	gameState->viewportAspect = config.viewportAspect;
	
	
	init_camera(&gameState->camera, 7.0, M_PI / 2, M_PI / 4, FALSE, 0, 0);
	
	initTextures(gameState);
	
	initModels(gameState);
	
	initSkysphere(&gameState->skysphere, &gameState->models[0], gameState->textures[2]);
	
	ColorRGB lightColor;
	initColorRGB(&lightColor, config.lightColorRed, config.lightColorGreen, config.lightColorBlue);
	Position3D lightPosition;
	initPosition3D(&lightPosition, config.lightPosX, config.lightPosY, config.lightPosZ);
	initLight(&gameState->light, 1.0, lightColor, lightPosition, GL_LIGHT0);
	
	initChessBoard(gameState->chessBoardCells, gameState->textures[0], gameState->textures[1]);
	initChessFigures(gameState);
	calculateValidMoves(gameState->chessBoardCells, gameState->figures, &gameState->whosTurn);
}

void initModels(GameState* gameState) {
	load_model("Models/Skysphere/Skysphere.obj", &gameState->models[0]);
	scale_model(&gameState->models[0], 50, 50, 50);
	
	load_model("Models/Rook/Rook_v2.obj", &gameState->models[1]);
	scale_model(&gameState->models[1], 0.2, 0.2, 0.2);
	
	load_model("Models/Knight/Knight_v2.obj", &gameState->models[2]);
	scale_model(&gameState->models[2], 0.2, 0.2, 0.2);
	
	load_model("Models/Bishop/Bishop_v2.obj", &gameState->models[3]);
	scale_model(&gameState->models[3], 0.2, 0.2, 0.2);
	
	load_model("Models/Queen/Queen_v2.obj", &gameState->models[4]);
	scale_model(&gameState->models[4], 0.2, 0.2, 0.2);
	
	load_model("Models/King/King_v2.obj", &gameState->models[5]);
	scale_model(&gameState->models[5], 0.2, 0.2, 0.2);
	
	load_model("Models/Pawn/Pawn_v2.obj", &gameState->models[6]);
	scale_model(&gameState->models[6], 0.2, 0.2, 0.2);
}

void initChessFigures(GameState* gameState) {
	initChessFigure( &gameState->figures[0], &gameState->models[1], gameState->textures[4], ROOK, TEAM_WHITE);
	initChessFigure( &gameState->figures[1], &gameState->models[2], gameState->textures[6], KNIGHT, TEAM_WHITE);
	initChessFigure( &gameState->figures[2], &gameState->models[3], gameState->textures[8], BISHOP, TEAM_WHITE);
	initChessFigure( &gameState->figures[3], &gameState->models[4], gameState->textures[10], QUEEN, TEAM_WHITE);
	initChessFigure( &gameState->figures[4], &gameState->models[5], gameState->textures[12], KING, TEAM_WHITE);
	initChessFigure( &gameState->figures[5], &gameState->models[3], gameState->textures[8], BISHOP, TEAM_WHITE);
	initChessFigure( &gameState->figures[6], &gameState->models[2], gameState->textures[6], KNIGHT, TEAM_WHITE);
	initChessFigure( &gameState->figures[7], &gameState->models[1], gameState->textures[4], ROOK, TEAM_WHITE);	
	initChessFigure( &gameState->figures[8], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[9], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[10], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[11], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[12], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[13], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[14], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	initChessFigure( &gameState->figures[15], &gameState->models[6], gameState->textures[14], PAWN, TEAM_WHITE);
	
	initChessFigure( &gameState->figures[31], &gameState->models[1], gameState->textures[3], ROOK, TEAM_BLACK);
	initChessFigure( &gameState->figures[30], &gameState->models[2], gameState->textures[5], KNIGHT, TEAM_BLACK);
	initChessFigure( &gameState->figures[29], &gameState->models[3], gameState->textures[7], BISHOP, TEAM_BLACK);
	initChessFigure( &gameState->figures[27], &gameState->models[4], gameState->textures[9], QUEEN, TEAM_BLACK);
	initChessFigure( &gameState->figures[28], &gameState->models[5], gameState->textures[11], KING, TEAM_BLACK);
	initChessFigure( &gameState->figures[26], &gameState->models[3], gameState->textures[7], BISHOP, TEAM_BLACK);
	initChessFigure( &gameState->figures[25], &gameState->models[2], gameState->textures[5], KNIGHT, TEAM_BLACK);
	initChessFigure( &gameState->figures[24], &gameState->models[1], gameState->textures[3], ROOK, TEAM_BLACK);	
	initChessFigure( &gameState->figures[23], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[22], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[21], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[20], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[19], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[18], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[17], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
	initChessFigure( &gameState->figures[16], &gameState->models[6], gameState->textures[13], PAWN, TEAM_BLACK);
}

void initTextures(GameState* gameState) {
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	loadTexture(&gameState->textures[2], "Textures/Skysphere/Skysphere.jpg");
	
	loadTexture(&gameState->textures[0], "Textures/Board/Board_b.png");
	loadTexture(&gameState->textures[1], "Textures/Board/Board_w.png");
		
	loadTexture(&gameState->textures[3], "Textures/Rook/Rook_b.png");
	loadTexture(&gameState->textures[4], "Textures/Rook/Rook_w.png");
	loadTexture(&gameState->textures[5], "Textures/Knight/Knight_b.png");
	loadTexture(&gameState->textures[6], "Textures/Knight/Knight_w.png");
	loadTexture(&gameState->textures[7], "Textures/Bishop/Bishop_b.png");
	loadTexture(&gameState->textures[8], "Textures/Bishop/Bishop_w.png");
	loadTexture(&gameState->textures[9], "Textures/Queen/Queen_b.png");
	loadTexture(&gameState->textures[10], "Textures/Queen/Queen_w.png");
	loadTexture(&gameState->textures[11], "Textures/King/King_b.png");
	loadTexture(&gameState->textures[12], "Textures/King/King_w.png");
	loadTexture(&gameState->textures[13], "Textures/Pawn/Pawn_b.png");
	loadTexture(&gameState->textures[14], "Textures/Pawn/Pawn_w.png");
	loadTexture(&gameState->textures[15], "Textures/ScreenMessages/Help_menu.png");
	loadTexture(&gameState->textures[16], "Textures/ScreenMessages/Check_message.png");
	loadTexture(&gameState->textures[17], "Textures/ScreenMessages/Check_mate_message.png");
	loadTexture(&gameState->textures[18], "Textures/ScreenMessages/Draw_message.png");
	
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glEnable(GL_TEXTURE_2D);
}
