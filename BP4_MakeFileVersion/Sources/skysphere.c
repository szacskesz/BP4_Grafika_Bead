#include <GL/glut.h>

#include "skysphere.h"
#include "model.h"
#include "texture.h"
#include "draw.h"


void renderSkysphere(Skysphere* skysphere) {
	glDisable(GL_LIGHTING);
	GLfloat col[] = {1.0, 1.0, 1.0};
	GLfloat col2[] = {0.0, 0.0, 0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, col);
		
	glPushMatrix();
		glEnable(GL_STENCIL_TEST);
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		glStencilFunc(GL_ALWAYS, 0, -1);
		glBindTexture(GL_TEXTURE_2D, skysphere->textureId);
		
		draw_model(skysphere->model);
		
		glDisable(GL_STENCIL_TEST);
	glPopMatrix();	
	
	glMaterialfv(GL_FRONT, GL_EMISSION, col2);
	glEnable(GL_LIGHTING);
}

void initSkysphere(Skysphere* skysphere, Model* model, Texture texture) {
	skysphere->textureId = texture.id;
	skysphere->model = model;
}
