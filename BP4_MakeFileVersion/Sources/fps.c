#include <GL/glut.h>
#include <stdio.h>

#include "fps.h"


void printFPS(int currentTime) {
	static int frame = 0;
	static int baseTime = 0;
	
	if (currentTime - baseTime > 1000) {
		frame++;
		printf("FPS: %4.2f\n", frame*1000.0/(currentTime-baseTime));
	 	baseTime = currentTime;
		frame = 0;
	} else {
		frame++;
	}
}
