#include <GL/glut.h>
#include <stdbool.h>
#include <math.h>

#include "chess.h"
#include "model.h"
#include "draw.h"
#include "utils.h"
#include "texture.h"
#include "screen_message.h"


GLfloat redDiffuse[] = {5.0, 0.1, 0.1};
GLfloat blueDiffuse[] = {0.1, 0.1, 5.0};
GLfloat greenDiffuse[] = {0.1, 5.0, 0.1};
GLfloat defaultDiffuse[] = {0.8, 0.8, 0.8};


void drawCuboid( float posX, float posY, float posZ, float sizeX, float sizeY, float sizeZ, int objId) {
	glPushMatrix();
	glBegin(GL_QUADS);
	
	for(int sgn = 1; sgn >= -1; sgn = sgn - 2) {
		for(int side = 0; side < 3; side++) {
			double normalCoords[3] = {0.0, 0.0, 0.0};
			normalCoords[side] = sgn;
			
			for(int v = 0; v <= 1; v++) {
				for(int u = v; u <= v+1; u++) {
					double modV = v % 2;
					double modU = u % 2;
					glNormal3f(normalCoords[0], normalCoords[1], normalCoords[2]);
					glTexCoord2f(modU,modV);
					
					glVertex3f(
						posX + normalCoords[0] * sizeX / 2 + !normalCoords[0] * ((modU - 0.5) * sizeX),
						posY - sizeY / 2 + normalCoords[1] * sizeY / 2 + !normalCoords[1] * ((modV - 0.5) * sizeY),
						posZ + normalCoords[2] * sizeZ / 2 + !normalCoords[2] * (!normalCoords[0] * (modV - 0.5) * sizeZ + !normalCoords[1] * (modU - 0.5) * sizeZ)
					);
				}
			}
		}
	}
	glEnd();
	glPopMatrix();	
}

void renderBoardCell(ChessBoardCell* chessBoardCell, ChessFigure figures[32], GLfloat highlightColor[3]) {
	glBindTexture(GL_TEXTURE_2D, chessBoardCell->textureId);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_ALWAYS, chessBoardCell->id, -1);
		
		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, highlightColor);
	drawCuboid(chessBoardCell->posX, chessBoardCell->posY, chessBoardCell->posZ, 1, 0.1, 1, chessBoardCell->id);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultDiffuse);
	
	//render figure if exist
	if(chessBoardCell->figureId != -1) {
	
		glPushMatrix();
			glBindTexture(GL_TEXTURE_2D, figures[chessBoardCell->figureId].textureId);
			glTranslatef(chessBoardCell->posX, chessBoardCell->posY+0.002, chessBoardCell->posZ);
			if(chessBoardCell->figureId > 16) {
				glRotatef(180, 0, 1, 0);
			}
			draw_model(figures[chessBoardCell->figureId].model);
		glPopMatrix();	
	}
		
	glDisable(GL_STENCIL_TEST);
}

void renderBoard(ChessBoardCell chessBoardCells[8][8], int selectedRowIndex, int selectedColIndex, ChessFigure figures[32]) {
	for (int i = 0; i < 8; i = i + 1)
	{
		for (int j = 0; j < 8; j = j + 1)
		{
			if(chessBoardCells[i][j].clickeable == TRUE && (selectedRowIndex == i && selectedColIndex == j) ) {
				renderBoardCell(&chessBoardCells[i][j], figures, greenDiffuse);
			} else if (chessBoardCells[i][j].clickeable == TRUE && (selectedRowIndex != -1 && selectedColIndex != -1) && chessBoardCells[i][j].figureId == -1 ) {
				renderBoardCell(&chessBoardCells[i][j], figures, blueDiffuse);
			} else if (chessBoardCells[i][j].clickeable == TRUE && (selectedRowIndex != -1 && selectedColIndex != -1) && chessBoardCells[i][j].figureId != -1 ) {
				renderBoardCell(&chessBoardCells[i][j], figures, redDiffuse);
			} else {
				renderBoardCell(&chessBoardCells[i][j], figures, defaultDiffuse);
			}
		}
	}
	
	glBindTexture(GL_TEXTURE_2D, chessBoardCells[0]->textureId);
	drawCuboid(-4.2, -0.0, 0.2, 0.4, 0.1, 8.4, 0);
	drawCuboid(0.2, -0.0, 4.2, 8.4, 0.1, 0.4, 0);
	drawCuboid(4.2, -0.0, -0.2, 0.4, 0.1, 8.4, 0);
	drawCuboid(-0.2, -0.0, -4.2, 8.4, 0.1, 0.4, 0);
}

void initChessBoard(ChessBoardCell chessBoardCells[8][8], Texture blackBoardTex, Texture whiteBoardTex) {
	
	for (int i = 0; i < 8; i = i + 1)
	{
		for (int j = 0; j < 8; j = j + 1)
		{
			if( (i+j) % 2 == 0 ) {
				initChessBoardCell(&chessBoardCells[i][j], i, j, blackBoardTex);
			} else {
				initChessBoardCell(&chessBoardCells[i][j], i, j, whiteBoardTex);
			}
		}
	}
	
}

void initChessBoardCell(ChessBoardCell* chessBoardCell, int i, int j, Texture texture) {
	
	chessBoardCell->id = i*8+j+1;
	chessBoardCell->textureId = texture.id;
	chessBoardCell->posX = j-3.5;
	chessBoardCell->posY = 0;
	chessBoardCell->posZ = -i+3.5;
	if( (i*8+j+1) <= 16) {
		//white figures
		chessBoardCell->figureId = i*8+j;
		chessBoardCell->clickeable = TRUE;
	} else if( (i*8+j+1) >= 49) {
		//black figures
		chessBoardCell->figureId = i*8+j-32;
		chessBoardCell->clickeable = FALSE;
	} else {
		chessBoardCell->figureId = -1;
		chessBoardCell->clickeable = FALSE;
	}
	
}

void initChessFigure(ChessFigure* chessFigure, Model* model, Texture texture, FigureType type, Team team) {
	chessFigure->firstMove = TRUE;
	chessFigure->textureId = texture.id;
	chessFigure->type = type;
	chessFigure->team = team;
	bool matrix [8][8] =  {
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
		{FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE}
	};
	memcpy(chessFigure->moves, matrix, sizeof matrix);
	chessFigure->model = model;
}

void clearClickeableFlags(ChessBoardCell chessBoardCells[8][8]) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			chessBoardCells[i][j].clickeable = FALSE;
		}
	}
}

void deepCopy_ChessFigure_BackupContent(ChessFigure* from, ChessFigure* to) {
	for(int i = 0; i < 8; i++){
		for(int j = 0; j < 8; j++){
			to->moves[i][j] = from->moves[i][j];
		}
	}
}

void deepCopy_ChessBoardCell_BackupContent(ChessBoardCell* from, ChessBoardCell* to) {
	to->figureId = from->figureId;
	to->clickeable = from->clickeable;
}

void deepCopy_ChessFigures_BackupContent(ChessFigure from[32], ChessFigure to[32]) {
	for(int i = 0; i < 32; i++) {
		deepCopy_ChessFigure_BackupContent(&from[i], &to[i]);
	}
}

void deepCopy_ChessBoardCells_BackupContent(ChessBoardCell from[8][8], ChessBoardCell to[8][8]) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			deepCopy_ChessBoardCell_BackupContent(&from[i][j], &to[i][j]);
		}
	}
}

bool isCurrentStateCheck(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], Team* whosTurn) {
	// get own king indexes
	int kingRowIndex = -10;
	int kingColIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId != -1 && 
			   figures[chessBoardCells[i][j].figureId].type == KING && 
			   figures[chessBoardCells[i][j].figureId].team == *whosTurn) 
			{
				kingRowIndex = i;
				kingColIndex = j;
			}
		}
	}
	
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			//check for enemy figures
			if(chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != *whosTurn) {
				//check for king attack
				if(chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].moves[kingRowIndex][kingColIndex] == TRUE) {
					return TRUE;
				}
			}
		}
	}
	return FALSE;
}

bool isMoveValid(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId, int moveRowIndex, int moveColIndex) {
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	
	bool isCheck;
	//make backup of board
	ChessBoardCell chessBoardCells_bckp[8][8];
	ChessFigure figures_bckp[32];
	deepCopy_ChessBoardCells_BackupContent(chessBoardCells, chessBoardCells_bckp);
	deepCopy_ChessFigures_BackupContent(figures, figures_bckp);
	
	//make move
	chessBoardCells[moveRowIndex][moveColIndex].figureId = chessBoardCells[selectedRowIndex][selectedColIndex].figureId;
	chessBoardCells[selectedRowIndex][selectedColIndex].figureId = -1;
	
	//reaclculate moves in temporary board
	calculateMoves(chessBoardCells, figures);
	
	//check ischeck
	isCheck = isCurrentStateCheck(chessBoardCells, figures, &figures[figureId].team);
	
	//restore backup
	deepCopy_ChessBoardCells_BackupContent(chessBoardCells_bckp, chessBoardCells);
	deepCopy_ChessFigures_BackupContent(figures_bckp, figures);

	return !isCheck;
}

void validateMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32]) {
	for(int k = 0; k < 32; k++) {
		for(int i = 0; i <= 7; i++) {
			for(int j = 0; j <= 7; j++) {
				if(figures[k].moves[i][j] == TRUE) {
					figures[k].moves[i][j] = isMoveValid(chessBoardCells, figures, k, i, j);
				}
			}
		}
	}
}

void calculcatePawnMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			figures[figureId].moves[i][j] = FALSE;
		}
	}
	
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	if(selectedColIndex == -10  || selectedRowIndex == -10) {
		//already killed
		return;
	}
	
	
	int i, j;
	i = selectedRowIndex;
	j = selectedColIndex;
	if(figures[figureId].team == TEAM_WHITE) {
		i++;
	} else {
		i--;
	}
	if( i <= 7 && i >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
		if(figures[figureId].team == TEAM_WHITE) {
			i++;
		} else {
			i--;
		}
		if( i <= 7 && i >= 0 && chessBoardCells[i][j].figureId == -1 && figures[figureId].firstMove == TRUE) {
			figures[figureId].moves[i][j] = TRUE;
		}
	}
	i = selectedRowIndex;
	j = selectedColIndex + 1;
	if(figures[figureId].team == TEAM_WHITE) {
		i++;
	} else {
		i--;
	}
	if( i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	j = selectedColIndex - 1;
	if( i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
}

void calculcateKnightMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			figures[figureId].moves[i][j] = FALSE;
		}
	}
	
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	if(selectedColIndex == -10  || selectedRowIndex == -10) {
		//already killed
		return;
	}

	int i, j;
	
	//+2 +1
	i = selectedRowIndex + 2;
	j = selectedColIndex + 1;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//+2 -1
	i = selectedRowIndex + 2;
	j = selectedColIndex - 1;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//+1 +2
	i = selectedRowIndex + 1;
	j = selectedColIndex + 2;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//+1 -2
	i = selectedRowIndex + 1;
	j = selectedColIndex - 2;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//-1 +2
	i = selectedRowIndex - 1;
	j = selectedColIndex + 2;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//-1 -2
	i = selectedRowIndex - 1;
	j = selectedColIndex - 2;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//-2 +1
	i = selectedRowIndex - 2;
	j = selectedColIndex + 1;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	//-2 -1
	i = selectedRowIndex - 2;
	j = selectedColIndex - 1;
	if(i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i <= 7 && i >= 0 && j <= 7 && j >= 0 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
}

void calculcateRookMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			figures[figureId].moves[i][j] = FALSE;
		}
	}
	
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	if(selectedColIndex == -10  || selectedRowIndex == -10) {
		//already killed
		return;
	}

	for(int i = selectedRowIndex + 1; i <= 7; i++) {
		if(chessBoardCells[i][selectedColIndex].figureId == -1) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team != figures[figureId].team) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
			break;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team == figures[figureId].team) {
			break;
		}
	}
	for(int i = selectedRowIndex - 1; i >= 0; i--) {
		if(chessBoardCells[i][selectedColIndex].figureId == -1) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team != figures[figureId].team) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
			break;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team == figures[figureId].team) {
			break;
		}
	}
	for(int j = selectedColIndex + 1; j <= 7; j++) {
		if(chessBoardCells[selectedRowIndex][j].figureId == -1) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team != figures[figureId].team) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
			break;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team == figures[figureId].team) {
			break;
		}
	}
	for(int j = selectedColIndex - 1; j >=0; j--) {
		if(chessBoardCells[selectedRowIndex][j].figureId == -1) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team != figures[figureId].team) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
			break;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team == figures[figureId].team) {
			break;
		}
	}
}

void calculcateBishopMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			figures[figureId].moves[i][j] = FALSE;
		}
	}
	
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	if(selectedColIndex == -10  || selectedRowIndex == -10) {
		//already killed
		return;
	}

	for(int i = 1; selectedRowIndex + i <= 7 && selectedColIndex + i <= 7; i++) {
		
		if(chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex + i] = TRUE;
		}
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId != -1 && 
			    figures[chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId].team != figures[figureId].team) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex + i] = TRUE;
			break;
		}
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId != -1 && 
		        figures[chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId].team == figures[figureId].team) 
		{
			break;
		}

	}
	for(int i = 1; selectedRowIndex + i <= 7 && selectedColIndex - i >= 0; i++) {
		
		if(chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex - i] = TRUE;
		}
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId].team != figures[figureId].team)
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex - i] = TRUE;
			break;
		}
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId].team == figures[figureId].team) 
		{
			break;
		}

	}
	for(int i = 1; selectedRowIndex - i >= 0 && selectedColIndex - i >= 0; i++) {
		
		if(chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId == -1)
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex - i] = TRUE;
		}
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId].team != figures[figureId].team)
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex - i] = TRUE;
			break;
		}
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId].team == figures[figureId].team)
		{
			break;
		}

	}
	for(int i = 1; selectedRowIndex - i >= 0 && selectedColIndex + i <= 7; i++) {
		
		if(chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex + i] = TRUE;
		}
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId].team != figures[figureId].team) 
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex + i] = TRUE;
			break;
		}
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId].team == figures[figureId].team)
		{
			break;
		}
	}
}

void calculcateQueenMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			figures[figureId].moves[i][j] = FALSE;
		}
	}
	
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	if(selectedColIndex == -10  || selectedRowIndex == -10) {
		//already killed
		return;
	}

	//from bishop
	for(int i = 1; selectedRowIndex + i <= 7 && selectedColIndex + i <= 7; i++) {
		
		if(chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex + i] = TRUE;
		}
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId].team != figures[figureId].team) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex + i] = TRUE;
			break;
		} 
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex + i][selectedColIndex + i].figureId].team == figures[figureId].team) 
		{
			break;
		}

	}
	for(int i = 1; selectedRowIndex + i <= 7 && selectedColIndex - i >= 0; i++) {
		
		if(chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex - i] = TRUE;
		} 
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId].team != figures[figureId].team) 
		{
			figures[figureId].moves[selectedRowIndex + i][selectedColIndex - i] = TRUE;
			break;
		} 
		else if(chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex + i][selectedColIndex - i].figureId].team == figures[figureId].team) 
		{
			break;
		}

	}
	for(int i = 1; selectedRowIndex - i >= 0 && selectedColIndex - i >= 0; i++) {
		
		if(chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex - i] = TRUE;
		} 
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId].team != figures[figureId].team) 
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex - i] = TRUE;
			break;
		} 
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex - i].figureId].team == figures[figureId].team) 
		{
			break;
		}

	}
	for(int i = 1; selectedRowIndex - i >= 0 && selectedColIndex + i <= 7; i++) {
		
		if(chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId == -1) 
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex + i] = TRUE;
		} 
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId].team != figures[figureId].team) 
		{
			figures[figureId].moves[selectedRowIndex - i][selectedColIndex + i] = TRUE;
			break;
		} 
		else if(chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId != -1 && 
				figures[chessBoardCells[selectedRowIndex - i][selectedColIndex + i].figureId].team == figures[figureId].team) 
		{
			break;
		}

	}
	//from rook
	for(int i = selectedRowIndex + 1; i <= 7; i++) {
		if(chessBoardCells[i][selectedColIndex].figureId == -1) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team != figures[figureId].team) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
			break;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team == figures[figureId].team) {
			break;
		}
	}
	for(int i = selectedRowIndex - 1; i >= 0; i--) {
		if(chessBoardCells[i][selectedColIndex].figureId == -1) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team != figures[figureId].team) {
			figures[figureId].moves[i][selectedColIndex] = TRUE;
			break;
		} else if (chessBoardCells[i][selectedColIndex].figureId != -1 && figures[chessBoardCells[i][selectedColIndex].figureId].team == figures[figureId].team) {
			break;
		}
	}
	for(int j = selectedColIndex + 1; j <= 7; j++) {
		if(chessBoardCells[selectedRowIndex][j].figureId == -1) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team != figures[figureId].team) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
			break;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team == figures[figureId].team) {
			break;
		}
	}
	for(int j = selectedColIndex - 1; j >=0; j--) {
		if(chessBoardCells[selectedRowIndex][j].figureId == -1) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team != figures[figureId].team) {
			figures[figureId].moves[selectedRowIndex][j] = TRUE;
			break;
		} else if (chessBoardCells[selectedRowIndex][j].figureId != -1 && figures[chessBoardCells[selectedRowIndex][j].figureId].team == figures[figureId].team) {
			break;
		}
	}
	
}

void calculcateKingMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], int figureId) {
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			figures[figureId].moves[i][j] = FALSE;
		}
	}
	
	int selectedColIndex = -10;
	int selectedRowIndex = -10;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId == figureId) {
				selectedRowIndex = i;
				selectedColIndex = j;
			}
		}
	}
	if(selectedColIndex == -10  || selectedRowIndex == -10) {
		//already killed
		return;
	}

	int i, j;
	//+1 +1 
	i = selectedRowIndex + 1;
	j = selectedColIndex + 1;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//+1 -1 
	i = selectedRowIndex + 1;
	j = selectedColIndex - 1;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//-1 +1 
	i = selectedRowIndex - 1;
	j = selectedColIndex + 1;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//-1 -1 
	i = selectedRowIndex - 1;
	j = selectedColIndex - 1;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//+1 +0
	i = selectedRowIndex + 1;
	j = selectedColIndex + 0;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//-1 +0
	i = selectedRowIndex - 1;
	j = selectedColIndex + 0;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//+0 +1
	i = selectedRowIndex + 0;
	j = selectedColIndex + 1;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	//+0 -1
	i = selectedRowIndex + 0;
	j = selectedColIndex - 1;
	
	if(i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId == -1) {
		figures[figureId].moves[i][j] = TRUE;
	} else if (i >= 0 && i <= 7 && j >= 0 && j <= 7 && chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team != figures[figureId].team) {
		figures[figureId].moves[i][j] = TRUE;
	}
	
	
}

void calculateMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32]) {
	for(int i = 0; i < 32; i++) {
		switch(figures[i].type) {
			case ROOK:
				calculcateRookMoves(chessBoardCells, figures, i);
				break;
			case KNIGHT:
				calculcateKnightMoves(chessBoardCells, figures, i);
				break;
			case BISHOP:
				calculcateBishopMoves(chessBoardCells, figures, i);
				break;
			case QUEEN:
				calculcateQueenMoves(chessBoardCells, figures, i);
				break;
			case KING:
				calculcateKingMoves(chessBoardCells, figures, i);
				break;
			case PAWN:
				calculcatePawnMoves(chessBoardCells, figures, i);
				break;
		}
	}
}

void calculateValidMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], Team* whosTurn) {
	calculateMoves(chessBoardCells, figures);
	validateMoves(chessBoardCells, figures);
}

void selectEvent(ChessBoardCell chessBoardCells[8][8], 
				 ChessFigure figures[32], 
				 int* selectedRowIndex, 
				 int* selectedColIndex, 
				 int clickedRowIndex, 
				 int clickedColIndex, 
				 Team* whosTurn) 
{
	*selectedRowIndex = clickedRowIndex;
	*selectedColIndex = clickedColIndex;
	
	clearClickeableFlags(chessBoardCells);
	chessBoardCells[*selectedRowIndex][*selectedColIndex].clickeable = TRUE;
	
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(figures[chessBoardCells[clickedRowIndex][clickedColIndex].figureId].moves[i][j] == TRUE) {
				chessBoardCells[i][j].clickeable = TRUE;
			}
		}
	}
}

void deselectEvent(ChessBoardCell chessBoardCells[8][8], 
				   ChessFigure figures[32], 
				   int* selectedRowIndex, 
				   int* selectedColIndex, 
				   int clickedRowIndex, 
				   int clickedColIndex, 
				   Team* whosTurn) 
{
	*selectedRowIndex = -1;
	*selectedColIndex = -1;
	
	clearClickeableFlags(chessBoardCells);
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			if(chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team == *whosTurn) {
				chessBoardCells[i][j].clickeable = TRUE;
			}
		}
	}

}

void moveEvent(ChessBoardCell chessBoardCells[8][8], 
			   ChessFigure figures[32], 
			   int* selectedRowIndex, 
			   int* selectedColIndex, 
			   int clickedRowIndex, 
			   int clickedColIndex, 
			   Team* whosTurn, 
			   bool* existRunningAnimation, 
			   AnimatedFigure animatedFigures[2]) 
{
	//figure to move
	animatedFigures[0].figureId = chessBoardCells[*selectedRowIndex][*selectedColIndex].figureId;
	animatedFigures[0].startTime = glutGet(GLUT_ELAPSED_TIME);
	Position3D startPositionMove;
	initPosition3D(&startPositionMove, 
				   chessBoardCells[*selectedRowIndex][*selectedColIndex].posX, 
				   chessBoardCells[*selectedRowIndex][*selectedColIndex].posY, 
				   chessBoardCells[*selectedRowIndex][*selectedColIndex].posZ);
	Position3D currPositionMove;
	initPosition3D(&currPositionMove, 
	               chessBoardCells[*selectedRowIndex][*selectedColIndex].posX, 
				   chessBoardCells[*selectedRowIndex][*selectedColIndex].posY, 
				   chessBoardCells[*selectedRowIndex][*selectedColIndex].posZ);
	Position3D endPositionMove;
	initPosition3D(&endPositionMove, 
	               chessBoardCells[clickedRowIndex][clickedColIndex].posX, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posY, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posZ);
	animatedFigures[0].startPos = startPositionMove;
	animatedFigures[0].currPos = currPositionMove;
	animatedFigures[0].endPos = endPositionMove;
	animatedFigures[0].startCell = &chessBoardCells[*selectedColIndex][*selectedColIndex];
	animatedFigures[0].endCell = &chessBoardCells[clickedRowIndex][clickedColIndex];
	
	//figure to disappear if exist
	animatedFigures[1].figureId = chessBoardCells[clickedRowIndex][clickedColIndex].figureId;
	animatedFigures[1].startTime = glutGet(GLUT_ELAPSED_TIME);
	Position3D startPositionDie;
	initPosition3D(&startPositionDie, 
	               chessBoardCells[clickedRowIndex][clickedColIndex].posX, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posY, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posZ);
	Position3D currPositionDie;
	initPosition3D(&currPositionDie, 
	               chessBoardCells[clickedRowIndex][clickedColIndex].posX, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posY, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posZ);
	Position3D endPositionDie;
	initPosition3D(&endPositionDie, 
	               chessBoardCells[clickedRowIndex][clickedColIndex].posX, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posY - 5.0, 
				   chessBoardCells[clickedRowIndex][clickedColIndex].posZ);
	animatedFigures[1].startPos = startPositionDie;
	animatedFigures[1].currPos = currPositionDie;
	animatedFigures[1].endPos = endPositionDie;
	animatedFigures[1].startCell = &chessBoardCells[*selectedColIndex][*selectedColIndex];
	animatedFigures[1].endCell = NULL;
	
	//set existAnimations true
	*existRunningAnimation = TRUE;
	
	figures[chessBoardCells[*selectedRowIndex][*selectedColIndex].figureId].firstMove = FALSE;
	chessBoardCells[clickedRowIndex][clickedColIndex].figureId = -1;
	chessBoardCells[*selectedRowIndex][*selectedColIndex].figureId = -1;
	if(*whosTurn == TEAM_WHITE) {
		*whosTurn = TEAM_BLACK;
	} else {
		*whosTurn = TEAM_WHITE;
	}
	*selectedRowIndex = -1;
	*selectedColIndex = -1;
	
	clearClickeableFlags(chessBoardCells);
}

void updateClickeableCells(ChessBoardCell chessBoardCells[8][8], 
						   ChessFigure figures[32], 
						   int* selectedRowIndex, 
						   int* selectedColIndex, 
						   int clickedRowIndex, 
						   int clickedColIndex, 
						   Team* whosTurn, 
						   bool* whiteCheck, 
						   bool* blackCheck, 
						   bool* existRunningAnimation, 
						   AnimatedFigure animatedFigures[2]) 
{		
	if(chessBoardCells[clickedRowIndex][clickedColIndex].clickeable == TRUE) {
		if(*selectedRowIndex == clickedRowIndex && *selectedColIndex == clickedColIndex) {
			deselectEvent(chessBoardCells, figures, selectedRowIndex, selectedColIndex, clickedRowIndex, clickedColIndex, whosTurn);
		} else if (*selectedRowIndex == -1 && *selectedColIndex == -1) {
			selectEvent(chessBoardCells, figures, selectedRowIndex, selectedColIndex, clickedRowIndex, clickedColIndex, whosTurn);
		} else if (*selectedRowIndex != -1 && *selectedColIndex != -1) {
			moveEvent(chessBoardCells, figures, selectedRowIndex, selectedColIndex, clickedRowIndex, clickedColIndex, whosTurn, existRunningAnimation, animatedFigures);
		}
	}
}

bool existAnyValidMove(ChessFigure figures[32], Team* whosTurn) {
	for(int k = 0; k < 32; k++) {
		if(figures[k].team == *whosTurn) {
			for(int i = 0; i < 8; i++) {
				for(int j = 0; j < 8; j++) {
					if(figures[k].moves[i][j] == TRUE) { return TRUE;}
				}
			}
		}
	}
	return FALSE;
}

bool checkForCheck(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], Team* whosTurn, bool* whiteCheck, bool* blackCheck) {
	bool isCheck = isCurrentStateCheck(chessBoardCells, figures, whosTurn);
	if(*whosTurn == TEAM_WHITE) {
		*whiteCheck = isCheck;
	} else {
		*blackCheck = isCheck;
	}
	return isCheck;
}

bool checkForCheckmate(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], Team* whosTurn, bool* whiteCheck, bool* blackCheck) {
	if(*whosTurn == TEAM_WHITE && *whiteCheck == TRUE && existAnyValidMove(figures, whosTurn) == FALSE) {
		return TRUE;
	}
	if(*whosTurn == TEAM_BLACK && *blackCheck == TRUE && existAnyValidMove(figures, whosTurn) == FALSE) {
		return TRUE;
	}
	return FALSE;
}

bool checkForDraw(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], Team* whosTurn, bool* whiteCheck, bool* blackCheck) {
	if(*whosTurn == TEAM_WHITE && *whiteCheck == FALSE && existAnyValidMove(figures, whosTurn) == FALSE) {
		return TRUE;
	}
	if(*whosTurn == TEAM_BLACK && *blackCheck == FALSE && existAnyValidMove(figures, whosTurn) == FALSE) {
		return TRUE;
	}
	return FALSE;
}

void handleGamesStates(ChessBoardCell chessBoardCells[8][8], 
					   ChessFigure figures[32], 
					   Team* whosTurn, 
					   bool* whiteCheck, 
					   bool* blackCheck, 
					   ShownScreenMessage* shownMessage, 
					   int* screenMessageStartTime) 
{
	bool isCheck = checkForCheck(chessBoardCells, figures, whosTurn, whiteCheck, blackCheck);
	bool isCheckmate = checkForCheckmate(chessBoardCells, figures, whosTurn, whiteCheck, blackCheck);
	bool isDraw = checkForDraw(chessBoardCells, figures, whosTurn, whiteCheck, blackCheck);
	
	if(isDraw == TRUE) {
		*shownMessage = DRAW;
		*screenMessageStartTime = glutGet(GLUT_ELAPSED_TIME);
	}
	if(isCheck == TRUE && isCheckmate == FALSE) {
		*shownMessage = CHECK;
		*screenMessageStartTime = glutGet(GLUT_ELAPSED_TIME);
	}
	if(isCheckmate == TRUE) {
		*shownMessage = CHECK_MATE;
		*screenMessageStartTime = glutGet(GLUT_ELAPSED_TIME);
	}
}

void renderAnimatedMove(ChessFigure figures[32], AnimatedFigure animatedFigures[2]) {
	for(int i = 0; i < 2; i++) {
		if(animatedFigures[i].figureId != -1) {
			glPushMatrix();
				glBindTexture(GL_TEXTURE_2D, figures[animatedFigures[i].figureId].textureId);
				glTranslatef(animatedFigures[i].currPos.x, animatedFigures[i].currPos.y + 0.01, animatedFigures[i].currPos.z);
				if(animatedFigures[i].figureId > 16) {
					glRotatef(180, 0, 1, 0);
				}
				draw_model(figures[animatedFigures[i].figureId].model);
			glPopMatrix();	
		}
	}
}

double getBezierCoord(double t, double shiftValue, double startCoord, double endCoord) {			
	double newCoord;
	if(t <= 1.0) {
		newCoord = 	pow(1-t, 3) * startCoord 
					+ 3 * t * pow(1-t, 2) * (startCoord + shiftValue)
					+ 3 * pow(t, 2) * (1-t) * (endCoord + shiftValue)
					+ pow(t, 3) * endCoord;
	} else {
		newCoord = endCoord;
	}
		
	return newCoord;
}

void updateAnimatedFiguresPosition(ChessBoardCell chessBoardCells[8][8], 
								   ChessFigure figures[32], 
								   Team* whosTurn, 
								   AnimatedFigure animatedFigures[2], 
								   int currentTime, 
								   bool* whiteCheck, 
								   bool* blackCheck, 
								   bool* existRunningAnimation, 
								   ShownScreenMessage* shownMessage, 
								   int* screenMessageStartTime) 
{
	*existRunningAnimation = FALSE;
	
	if(animatedFigures[0].figureId != -1) {
		*existRunningAnimation = TRUE;
				
		double height = 3.0;
		double t = ((((double)currentTime - animatedFigures[0].startTime) / 1000) * animatedFigures[0].animationSpeed);
						
		if(t <= 1.0) {
			animatedFigures[0].currPos.x = getBezierCoord(t, 0.0, animatedFigures[0].startPos.x, animatedFigures[0].endPos.x);
			animatedFigures[0].currPos.y = getBezierCoord(t, height, animatedFigures[0].startPos.y, animatedFigures[0].endPos.y);
			animatedFigures[0].currPos.z = getBezierCoord(t, 0.0, animatedFigures[0].startPos.z, animatedFigures[0].endPos.z);
		} else {
			//arrived
			animatedFigures[0].currPos.x = animatedFigures[0].endPos.x;
			animatedFigures[0].currPos.y = animatedFigures[0].endPos.y;
			animatedFigures[0].currPos.z = animatedFigures[0].endPos.z;
			if(animatedFigures[0].endCell != NULL) {
				animatedFigures[0].endCell->figureId = animatedFigures[0].figureId;
			}
			animatedFigures[0].figureId = -1;
		}
	}
	if(animatedFigures[1].figureId != -1) {
		*existRunningAnimation = TRUE;
		double p = ((double)currentTime - animatedFigures[1].startTime) / 1000;
					
		Position3D moveVector;
		initPosition3D(&moveVector, 
					   animatedFigures[1].endPos.x - animatedFigures[1].startPos.x, 
					   animatedFigures[1].endPos.y - animatedFigures[1].startPos.y, 
					   animatedFigures[1].endPos.z - animatedFigures[1].startPos.z);
		double moveVectorLength = (double)sqrt((moveVector.x * moveVector.x) + (moveVector.y * moveVector.y) + (moveVector.z * moveVector.z));
		moveVector.x = moveVector.x / moveVectorLength;
		moveVector.y = moveVector.y / moveVectorLength;
		moveVector.z = moveVector.z / moveVectorLength;
		
		
		double multiplicant = (double)p * animatedFigures[1].animationSpeed;
		if(multiplicant <= moveVectorLength) {
			animatedFigures[1].currPos.x = animatedFigures[1].startPos.x + multiplicant * moveVector.x;
			animatedFigures[1].currPos.y = animatedFigures[1].startPos.y + multiplicant * moveVector.y;	
			animatedFigures[1].currPos.z = animatedFigures[1].startPos.z + multiplicant * moveVector.z;
		} else {
			//arrived
			multiplicant = moveVectorLength;
			animatedFigures[1].currPos.x = animatedFigures[1].startPos.x + multiplicant * moveVector.x ;
			animatedFigures[1].currPos.y = animatedFigures[1].startPos.y + multiplicant * moveVector.y;
			animatedFigures[1].currPos.z = animatedFigures[1].startPos.z + multiplicant * moveVector.z;
			if(animatedFigures[1].endCell != NULL) {
				animatedFigures[1].endCell->figureId = animatedFigures[1].figureId;
			}
			animatedFigures[1].figureId = -1;
		}
	}
		
	if(*existRunningAnimation == FALSE) {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				if(chessBoardCells[i][j].figureId != -1 && figures[chessBoardCells[i][j].figureId].team == *whosTurn) {
					chessBoardCells[i][j].clickeable = TRUE;
				}
			}
		}
		calculateValidMoves(chessBoardCells, figures, whosTurn);
		handleGamesStates(chessBoardCells, figures, whosTurn, whiteCheck, blackCheck, shownMessage, screenMessageStartTime);
	}
}
