#include <GL/glut.h>
#include <stdbool.h>

#include "init.h"
#include "callbacks.h"
#include "gamestate.h"


GameState gameState;

int main(int argc, char* argv[]) {
	
	Config config;
	loadDefaultConfig(&config);
	loadConfigFromFile(&config, "config.ini");
		
	glutInit(&argc, argv);
	glutInitWindowSize(config.screenWidth, config.screenHeight);     
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
	int window = glutCreateWindow("FreeChess By Szacskesz");
	glutSetWindow(window);

	initialize(&gameState, config);
	
	setCallbacks();

    glutMainLoop();
    return 0;
}
