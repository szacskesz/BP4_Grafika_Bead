#ifndef CHESS_H
#define CHESS_H

#include <GL/glut.h>
#include <stdbool.h>
#include "model.h"
#include "utils.h"
#include "texture.h"
#include "screen_message.h"


typedef enum {
	ROOK, 
	KNIGHT, 
	BISHOP, 
	QUEEN, 
	KING, 
	PAWN
} FigureType;

typedef struct ChessBoardCell { 
	int id;
   	double posX;
	double posY;
	double posZ;
	int figureId;
	int textureId;
	bool clickeable;
} ChessBoardCell;

typedef struct ChessFigure { 
	bool firstMove;
	Model* model;
	int textureId;
	Team team;
	FigureType type;
	bool moves[8][8];
} ChessFigure;

typedef struct AnimatedFigure {
	int figureId;
	int startTime;
	int animationSpeed; // units/second
    Position3D startPos;
	Position3D currPos;
	Position3D endPos;
	ChessBoardCell* startCell;
	ChessBoardCell* endCell;
} AnimatedFigure;


/**
 * Draws a cuboid at the given (1-3. param) position and given sizes (4-6. param)
 * 	with the objId as stencil id (identifies the clicked object).
 */
void drawCuboid( float posX, float posY, float posZ, float sizeX, float sizeY, float sizeZ, int objId);

/**
 * Draws the board cell with the proper texture and the corresponding figure if any exists.
 */
void renderBoardCell(ChessBoardCell* chessBoardCell, ChessFigure figures[32], GLfloat highlightColor[3]);

/**
 * Draws every board cell and the board's frame wiht proper textures.
 */
void renderBoard(ChessBoardCell chessBoardCells[8][8], int selectedRowIndex, int selectedColIndex, ChessFigure figures[32]);

/**
 * Initializes the chessboard's cells.
 */
void initChessBoard(ChessBoardCell chessBoardCells[8][8], Texture whiteBoardTex, Texture blackBoardTex);

/**
 * Initializes the chessboard cell's default values, set figures position too if needed.
 */
void initChessBoardCell(ChessBoardCell* chessBoardCell, int i, int j, Texture texture);

/**
 * Initializes the figure default values, it's model, texture, type, and team.
 */
void initChessFigure(ChessFigure* chessFigure, Model* model, Texture texture, FigureType type, Team team);

/**
 * Updates the chessboard cell's clickeable state based on the possible valid moves calculated before.
 */
void updateClickeableCells(ChessBoardCell chessBoardCells[8][8],
                           ChessFigure figures[32],
                           int* selectedRowIndex, int* selectedColIndex,
                           int clickedRowIndex, int clickedColIndex,
                           Team* whosTurn,
                           bool* whiteCheck, bool* blackCheck,
                           bool* existRunningAnimation,
                           AnimatedFigure animatedFigures[2]);

/**
 * Calculates every chess figure's possible, valid moves (considering checks).
 */
void calculateValidMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32], Team* whosTurn);

/**
 * Calculates every chess figure's possible moves without being aware of checks.
 */
void calculateMoves(ChessBoardCell chessBoardCells[8][8], ChessFigure figures[32]);

/**
 * Checks black and white player's check state and write result to console.
 */
void handleGamesStates(ChessBoardCell chessBoardCells[8][8], 
					   ChessFigure figures[32], 
					   Team* whosTurn, 
					   bool* whiteCheck, 
					   bool* blackCheck, 
					   ShownScreenMessage* shownMessage, 
					   int* screenMessageStartTime);

/**
 * Draw the moving figures.
 */
void renderAnimatedMove(ChessFigure figures[32], AnimatedFigure animatedFigures[2]);

/**
 * Updates the moving figures's position over time.
 */
void updateAnimatedFiguresPosition(ChessBoardCell chessBoardCells[8][8],
                                   ChessFigure figures[32],
                                   Team* whosTurn,
                                   AnimatedFigure animatedFigures[2],
                                   int currentTime,
                                   bool* whiteCheck, bool* blackCheck,
                                   bool* existRunningAnimation,
                                   ShownScreenMessage* shownMessage,
                                   int* screenMessageStartTime);

								   
#endif /* CHESS_H */
