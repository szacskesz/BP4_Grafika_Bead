#ifndef INIT_H
#define INIT_H

#include "gamestate.h"


typedef struct Config {
	int screenMessageDuration;
	double dieAnimationSpeed;
	double moveAnimationSpeed;
	bool showFps;
	double lightPosX;
	double lightPosY;
	double lightPosZ;
	double lightColorRed;
	double lightColorGreen;
	double lightColorBlue;
	int screenWidth;
	int screenHeight;
	double viewportRatioW;
	double viewportRatioH;
	double viewportAspect;
} Config;

/**
 * Initializes the configuration with the default values.
 */
void loadDefaultConfig(Config* config);

/**
 * Loads the configuration file and applies it to the current configuration.
 */
void loadConfigFromFile(Config* config, const char filename[100]);

/**
 * Initializes the game and the openGL context.
 */
void initialize(GameState* gameState, Config config);

/**
 * Initializes the gamestate object with default values.
 */
void initGameSate(GameState* gameState, Config config);

/**
 * Loads the models into memory and scales them to the proper sizes.
 */
void initModels(GameState* gameState);

/**
 * Initializes all the chess figures with default the values.
 */
void initChessFigures(GameState* gameState);

/**
 * Loads and initializes the textures for use.
 */
void initTextures(GameState* gameState);


#endif /* INIT_H */
