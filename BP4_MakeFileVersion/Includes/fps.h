#ifndef FPS_H
#define FPS_H


/**
 * Prints the application's frames per second (FPS).
 */
void printFPS(int currentTime);


#endif /* FPS_H */
