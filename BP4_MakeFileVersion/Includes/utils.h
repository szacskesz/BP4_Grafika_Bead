#ifndef UTILS_H
#define UTILS_H


typedef struct Position3D { 
    double x;
	double y;
	double z;
} Position3D;

typedef struct Size3D { 
    double sizeX;
	double sizeY;
	double sizeZ;
} Size3D;

typedef struct ColorRGB { 
    double R;
	double G;
	double B;
} ColorRGB;

typedef enum {TEAM_WHITE, TEAM_BLACK} Team;

/**
 * Initializes the Position3D (1. param) with values (2-4. params)
 */
void initPosition3D(Position3D* pos, double x, double y, double z);

/**
 * Initializes the initSize3D (1. param) with values (2-4. params)
 */
void initSize3D(Size3D* sizes, double sizeX, double sizeY, double sizeZ);

/**
 * Initializes the initColorRGB (1. param) with values (2-4. params)
 */
void initColorRGB(ColorRGB* color, double R, double G, double B);


#endif /* UTILS_H */
