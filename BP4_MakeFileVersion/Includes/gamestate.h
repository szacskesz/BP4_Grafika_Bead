#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "lightning.h"
#include "camera.h"
#include "texture.h"
#include "model.h"
#include "skysphere.h"
#include "chess.h"
#include "screen_message.h"
#include "utils.h"


/**
 * Game state representation
 */
typedef struct GameState
{
    Light light;
	Camera camera;
	Texture textures[19];
	Model models[7];
	Skysphere skysphere;
	ChessFigure figures[32];
	ChessBoardCell chessBoardCells[8][8];
	AnimatedFigure animatedFigures[2];

	Team whosTurn;
	bool whiteCheck;
	bool blackCheck;
	bool existRunningAnimation;
	int selectedRowIndex;
	int selectedColIndex;
	ShownScreenMessage shownMessage;
	int screenMessageDuration;
	int screenMessageStartTime;
	bool showFps;
	double viewportRatioW;
	double viewportRatioH;
	double viewportAspect;
} GameState;


#endif /* GAMESTATE_H */
