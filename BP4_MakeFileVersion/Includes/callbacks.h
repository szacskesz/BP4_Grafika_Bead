#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <GL/glut.h>


/**
 * Sets GLUT callbacks to the corresponding functions.
 */
void setCallbacks();

/**
 * Call when need to display the graphical content.
 */
void display();

/**
 * Call after windows resizing.
 */
void reshape(GLsizei width, GLsizei height);

/**
 * Call after mouse click event.
 */
void mouseHandler(int button, int state, int x, int y);

/**
 * Call after mouse scroll event.
 */
void mouseScrollHandler(int wheel, int direction, int x, int y);

/**
 * Call after mouse motion event.
 */
void mouseMotion(int x, int y);

/**
 * Call after keyboard event.
 */
void keyboardNormalButtonHandler(unsigned char key, int x, int y);

/**
 * Call after keyboard function button event.
 */
void keyboardSpecialButtonHandler (int key, int x, int y);

/**
 * Call when there is no other event.
 */
void idle();


#endif /* CALLBACKS_H */
