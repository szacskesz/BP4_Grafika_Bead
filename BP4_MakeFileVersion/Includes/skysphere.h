#ifndef INCLUDES_H
#define INCLUDES_H

#include "model.h"
#include "texture.h"


typedef struct Skysphere { 
	Model* model;
	int textureId;
} Skysphere;

/**
 * Draws the skysphere (1. param) with disabled lightning, proper material and default stencil id.
 */
void renderSkysphere(Skysphere* skysphere);

/**
 * Sets the skysphere's (1. param) model (2. param) and texture (3. param).
 */
void initSkysphere(Skysphere* skysphere, Model* model, Texture texture);


#endif /* INCLUDES_H */
