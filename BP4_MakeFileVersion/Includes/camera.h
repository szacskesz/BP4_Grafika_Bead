#ifndef CAMERA_H
#define CAMERA_H

#include <stdbool.h>


typedef struct Camera { 
    double zoomFactor;	
	double horizontalAngleRadian;
	double verticalAngleRadian;	
	bool startCapturingMouseMovement;
	double mouseOriginalPositionX;
	double mouseOriginalPositionY;
} Camera;


/**
 * Initializes the camera object with the given values (2-6. param)
 * 
 * where horizontalAngleRadian: is the angle between the X axis and the (x,0,z) vector made of the cameras coordinates   
 * where verticalAngleRadian: is the angle between the X,Y surface and the (x,y,z) vector made of the cameras coordinates   
 */
void init_camera(Camera* camera,
                 double zoomFactor,
                 double horizontalAngleRadian,
                 double verticalAngleRadian,
                 bool startCapturingMouseMovement,
                 double mouseOriginalPositionX,
                 double mouseOriginalPositionY);

/**
 * Draws the camera in the spherical coordiante system.
 */
void renderCamera(Camera* camera);

/**
 * Changes the camera's horizontalAngleRadian and/or verticalAngleRadian value, so rotates the camera
 */
void processCameraRotationEvent(int x, int y, Camera* camera);

/**
 * Checks the pressed button so rotation event only occur when proper button hold down.
 */
void checkCameraRotationEvent(int button, int state, int x, int y, Camera* camera);

/**
 * Zooms the camera in or out, (changes the camera's position closer or farther compared to the origo).
 */
void processCameraZoomEvent(int direction, Camera* camera);

/**
 * Prints the camera object current values to console.
 */
void printCameraDetail(Camera* camera);


#endif /* CAMERA_H */
