#ifndef LIGHTNING_H
#define LIGHTNING_H

#include <GL/glut.h>
#include "utils.h"


typedef struct Light { 
    double intensity;
	ColorRGB color;
	Position3D position;
	GLenum id;
} Light;


void initLight(
	Light* light,
	double intensity,
	ColorRGB color,
	Position3D position,
	GLenum id
);

/**
 * Changes the light's intensity by 0.05/button hit.
 */
void changeLightIntensity(unsigned char key, Light* light);

/**
 * Changes every light's intensity in the light array.
 */
void changeLightsIntensity(unsigned char key, Light lights[], int size);

/**
 * Draws the light to it's position wiht color, and intensity.
 */
void renderLight(Light* light);

/**
 * Draws every light from the light array.
 */
void renderLights(Light lights[], int size);

/**
 *  Prints the light object current values to console.
 */
void printLightDetail(Light* light);

/**
 * Prints every light array member's current values to console.
 */
void printLightsDetail(Light lights[], int size);


#endif /* LIGHTNING_H */
