#ifndef SCREEN_MESSAGE_H
#define SCREEN_MESSAGE_H


typedef enum {
	NONE = -1, 
	HELP = 15, 
	CHECK = 16, 
	CHECK_MATE = 17, 
	DRAW = 18
} ShownScreenMessage;

/**
 * Toggles the visiblity of the help menu if no other message displayed.
 */
void toggleHelpMenu(int key, ShownScreenMessage* shownMessage);

/**
 * Renders the given texture message to the screen.
 */
void renderScreenMessage(Texture shownScreenMessageTexture);

/**
 * Hides the screen message after a specific wait time.
 */
void updateScreenMessageVisiblity(int currentTime,
                                  ShownScreenMessage* shownMessage,
                                  int* screenMessageStartTime,
                                  int screenMessageDuration);

								  
#endif /* SCREEN_MESSAGE_H */
